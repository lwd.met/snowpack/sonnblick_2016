/***********************************************************************************/
/* Copyright 2019-05 Michael Reisecker                                                */
/***********************************************************************************/
/* This is free software: you can redistribute and/or modify it under the terms of */
/* the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses   */
/***********************************************************************************/

/* SYNOPSIS: ./sonnblick */

/*
 * Control program for MeteoIO. This is only a test program to check the filtering
 * stage of this simulation without calling SNOWPACK. In the end, it is obsolete
 * for the experiment.
 */

#include <iostream>
#include <meteoio/MeteoIO.h>

using namespace std;
using namespace mio;

bool has_nodata(const MeteoData& md) { //does a data point contain any nodata values?
	for (size_t pp = 0; pp < md.getNrOfParameters(); ++pp) {
		if (md(pp) == IOUtils::nodata)
			return true;
	}
	return false;
}

int main(/* int argc, char** argv */) {
	Config cfg("input/io.ini");
	IOManager io(cfg);

	Date sdate, edate;
	IOUtils::convertString(sdate, "2016-09-01T00:00", 0.); //season 16/17
	IOUtils::convertString(edate, "2017-05-3T23:59", 0.);

	vector< vector<MeteoData> > mvec;

	cout << "Reading data..." << endl;
	io.getMeteoData(sdate, edate, mvec);
	cout << "Average sampling rate of dataset: 1 per " 
		 << 1./(io.getAvgSamplingRate()*60) << " min." << endl;

	const bool perform_resampling = true;
	if (perform_resampling) { //this does NOT resample to fixed intervals like SNOWPACK does
		cout << "Resampling..." << endl;
		Meteo1DInterpolator interpol(cfg);
		MeteoData md;

		for (size_t ii = 0; ii < mvec.size(); ++ii) {
			for (size_t jj = 0; jj < mvec[ii].size(); ++jj) {
				md = mvec[ii][jj]; //get meta values
				if ( has_nodata(mvec[ii][jj]) ) { //interpolate if any data point at this time step is nodata
					interpol.resampleData(mvec[ii][jj].date, mvec[ii], md);
					mvec[ii][jj] = md; //md is the resampled data point
				}
			} //endfor jj
		}
	}

	io.writeMeteoData(mvec); //write to sonnblick.smet (as opposed to *_forcing.smet from SNOWPACK)

	cout << "Done." << endl;
	return 0;
}
