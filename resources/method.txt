SNOWPACK [1] is a 1D multi-layer snowpack model developed at SLF for avalanche forecasting. It is driven by meteorological 
data that is filtered through a sophisticated preprocessor tailored to the needs of high-Alpine weather stations.

Solving mass and energy exchange equations between the snow cover, atmosphere, vegetation and soil, and treating mass 
and energy fluxes within these media, it can simulate the development of the snowpack during the winter and show modelled
snow profiles for a given location and time where meteorological data is available. For this, it takes into account snow 
metamorphisms, settling, phase changes, wind erosion, water and vapor transport, heat conduction, melting/refreezing, and more.

At the core, from the snow density, temperature and liquid water content, the microscopic parameters grain size, bond radius, 
sphericity and  dendricity are derived for each layer and from there the macroscopic parameters thermal conductivity and 
viscosity that are needed to compute the conservation equations. Several modules connect observable data to this microstructure.

In our simulation, we use the absolute snow height to calculate new snow sums. Neither the snow surface temperature nor the 
reflected short wave radiation are measured so for the upper boundary condition we use the incoming long wave radiation and 
parametrize the albedo. This implies working with fluxes rather than temperatures, i. e. Neumann boundary conditions. 
Details and further references are available at this project's git repository [2].

The end result is the temporal evolution of the snow cover as a timeseries of modelled snow profiles at our experiment site.

[1] Lehning, M., Bartelt, P., Brown, R.L., Russi, T., Stöckli, U., Zimmerli, M., Snowpack Model Calculations for 
Avalanche Warning based upon a new Network of Weather and Snow Stations, 1999, Cold Reg. Sci. Technol., 30, 145-157.
[2] https://gitlab.com/lwd.met/snowpack/sonnblick_2016
