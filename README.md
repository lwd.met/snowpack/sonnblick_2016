# Sonnblick 2016

Correlating a snow profile including snow probes (bacteria distribution) at the end of the winter season with simulated layers.
Trying to calculate which precipitation event is responsible for which snowpack layer.

The resulting paper can be found in /resources.

There is a wiki available at this project's git repository:  
https://gitlab.com/lwd.met/snowpack/sonnblick_2016/wikis/home
