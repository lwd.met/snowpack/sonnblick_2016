#!/bin/bash

#On Jan 10th 10:30 to 10:50 there is -30cm snow. A webcam shows a cold, beautiful day
#and there was unusually low wind. So, starting at this point we add back 30 cm.
#SYNOPSIS: ./offset_snow.sh <input_file> <output_file>

awk -F ',' '
	BEGIN { OFS = "," }
	NR<=18195 { print $0 }
	NR>18195 {
		if ($4 == -999)
			print $0
		else
			print $1,$2,$3, ($4-104 < -30)? 0 : ($4+30), $5
	}
' $1 > $2
