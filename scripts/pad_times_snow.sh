#!/bin/bash

#This script pads the time fields with a leading zero, e. g. 120 --> 0120 (01:20).
#SYNOPSIS: ./pad_times_snow <input_file> <output_file>

awk -F ',' '
	NR==1 { print $0 }
	NR>1 { printf "%i,%i,%04i,%i,%i\n", $1,$2,$3,$4,$5 }
' $1 >> $2
