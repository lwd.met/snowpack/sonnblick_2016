#!/bin/bash

#This script removes bad data according to ZAMG's automatic + manual data quality control (status flag 10).
#SYNOPSIS: ./remove_qa10_snow.sh <input_file> <output_file>

awk -F ',' '
	NR==1 { print $0 }
	NR>1 {
		if ($5 != 10) print $0
	}
' $1 >> $2
