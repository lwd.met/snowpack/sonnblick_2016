#!/bin/bash

#This script removes Windows newlines and should be called first on the original data.
#SYNOPSIS: ./rmnl.sh <input_file> <output_file>

sed -i 's/\r//g' $1 #remove Windows newlines
