#!/bin/bash

#This script pads the time fields with a leading zero, e. g. 10 --> 0010 (00:10).
#SYNOPSIS: ./pad_times_radiation <input_file> <output_file>

awk -F ';' '
	NR<=30 { print $0 }
	NR>30 { printf "%i;%i;%04i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i;%i\n", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17 }
' $1 >> $2
